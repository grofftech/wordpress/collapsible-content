<?php
/**
 * Runtime configuration for the qa shortcode.
 *
 * @package     GroffTech\ProjectName\Folder
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\CollapsibleContent\Config\Shortcode;

return array(
    /*======================================
     * The shortcode name [qa].
     *======================================*/
    'shortcode_name' => 'qa',

    /*======================================
     * Directory path to the view file.
     * For multiple view files, provide an array
     * of directory paths.
     *======================================*/
    'view' => COLLAPSIBLE_CONTENT_DIR . '/lib/shortcode/views/qa.php',

    /*======================================
     * Defined shortcode default attributes.
     * These can be overridden by the author
     * in content.
     *======================================*/
    'defaults' => array(
        'show_icon' => 'dashicons dashicons-arrow-down-alt2',
        'hide_icon' => 'dashicons dashicons-arrow-up-alt2',
        'question' => ''
    ),
);