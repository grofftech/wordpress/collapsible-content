<?php
/**
 * Configuration for the teaser shortcode.
 *
 * @package     GroffTech\CollapsibleContent\Config\Shortcode
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\CollapsibleContent\Config\Shortcode;

return array(
    /*======================================
     * The shortcode name [teaser].
     *======================================*/
    'shortcode_name' => 'teaser',

     /*======================================
     * Directory path to the view file.
     * For multiple view files, provide an array
     * of directory paths.
     *======================================*/
    'view' => COLLAPSIBLE_CONTENT_DIR . '/lib/shortcode/views/teaser.php',

    /*======================================
     * Defined shortcode default attributes.
     * These can be overridden by the author
     * in content
     *======================================*/
    'defaults' => array(
        'show_icon' => 'dashicons dashicons-arrow-down-alt2',
        'hide_icon' => 'dashicons dashicons-arrow-up-alt2',
        'visible_message' => ''
    ),
);