# Collapsible Content Plugin

A WordPress plugin to show teaser content with an option to show or hide hidden content. Practical examples include Q&A's and FAQ's and more.

## Requirements

- Node
- NPM
- Git
- Make

## Features

This plugin includes the following features:

- QA Shortcode '[qa]'
- Teaser Shortcode '[teaser]'
- Font icon visual indicator
- Vanilla JS sliding animation

## Installation

- In your terminal, navigate to the `wp-content/plugins` folder of your project.

- Clone the repository

    ```git
    git clone https://github.com/groffTECH/collapsible-content.git && cd collapsible-content
    ```

- Install Composer dependencies

    ```bash
    composer install
    ```

- Install Node dependencies

    ```bash
    npm install
    ```

- Build the code

    ```bash
    make all
    ```

- Login to the admin area your WordPress website.
- Navigate to Plugins and activate the Collapsible Content plugin.
