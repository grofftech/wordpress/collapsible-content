<?php
/**
 * Collapsible Content Plugin
 *
 * @package     GroffTech\CollapsibleContent
 * @author      Brett Groff (groffTECH)
 * @link        https://grofftech.net
 * @copyright   2016 Grofftech
 * @license     GNU General Public License 2.0+
 *
 * @wordpress-plugin
 * Plugin Name:  Collapsible Content
 * Plugin URI:   https://gitlab.com/grofftech/wordpress/collapsible-content
 * Description:  Show teaser content with an option to show or hide hidden content. Practical examples include Q&A's and FAQ's and more.
 * Version:      1.3.0
 * Author:       Grofftech
 * Author URI:   https://grofftech.net
 * Text Domain:  collapsible-content
 * License:      GPL-2.0+
 * License URI:  http: //www.gnu.org/licenses/gpl-2.0.txt
 * Requires PHP: 7.4
 */

namespace GroffTech\CollapsibleContent;

if (!defined('ABSPATH')) {
    exit("You are not WordPress!");
}

/**
* Define the required constants for the plugin.
*
* @since 1.0.0
*
* @return void
*/
function setupConstants() {
    define('COLLAPSIBLE_CONTENT_PLUGIN', __FILE__ );
    define('COLLAPSIBLE_CONTENT_DIR', \plugin_dir_path( __FILE__ ));

    $plugin_url = \plugin_dir_url( __FILE__ );
    if ( \is_ssl() ) {
        $plugin_url = str_replace( 'http://', 'https://', $plugin_url );
    }

    define('COLLAPSIBLE_CONTENT_URL', $plugin_url);
    define('COLLAPSIBLE_CONTENT_TEXT_DOMAIN', 'collapsible-content');
}

setupConstants();
include(__DIR__ . '/vendor/autoload.php');