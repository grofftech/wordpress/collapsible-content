(function(window, document) {
    "use strict";

    var init = function() {
        document.addEventListener("click", clickHandler);
    };

    var clickHandler = function(event) {
        var hiddenContent = null;
        var icon = null;

        // Polyfill matches if older browser support is needed
        if (event.target.matches(".collapsible-content--visible")) {
            hiddenContent = event.target.nextElementSibling;
            toggle(hiddenContent);

            icon = event.target.firstElementChild;
            toggleIcon(icon, hiddenContent);
        }

        if (event.target.matches(".collapsible-content--icon")) {
            hiddenContent = event.target.parentElement.nextElementSibling;
            toggle(hiddenContent);

            icon = event.target;
            toggleIcon(icon, hiddenContent);
        }
    };

    var show = function(element, displayType) {
        element.style.display = displayType;
    };

    var hide = function(element) {
        element.style.display = "none";
    };

    // TODO: Animate this
    var toggle = function(element, displayType) {
        displayType = displayType || "block";

        if (window.getComputedStyle(element).display === displayType) {
            hide(element);
        } else {
            show(element, displayType);
        }
    };

    var toggleIcon = function(icon, element, displayType) {
        displayType = displayType || "block";

        var showIcon = icon.attributes["data-show-icon"].value;
        var showIconClasses = showIcon.split(" ");

        var hideIcon = icon.attributes["data-hide-icon"].value;
        var hideIconClasses = hideIcon.split(" ");

        if (window.getComputedStyle(element).display == displayType) {
            // Content is showing
            // remove the show icon and add the hide icon
            showIconClasses.forEach(function(item) {
                icon.classList.remove(item);
            });

            hideIconClasses.forEach(function(item) {
                icon.classList.add(item);
            });
        } else {
            // Content is not showing
            // remove the hide icon and add the show icon
            hideIconClasses.forEach(function(item) {
                icon.classList.remove(item);
            });

            showIconClasses.forEach(function(item) {
                icon.classList.add(item);
            });
        }
    };


    init();
})(window, document);