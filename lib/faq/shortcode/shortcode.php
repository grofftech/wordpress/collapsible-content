<?php

/**
 * FAQ Shortcode processing
 *
 * @package     GroffTech\Module\FAQ\ShortCode
 * @since       1.3.0
 * @author      Brett Groff (groffTECH)
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\FAQ\ShortCode;

/**
 * Processes the shortcode.
 *
 * @param array The shortcode configuration parameters.
 * @param string $attributes The attributes for the shortcode instance.
 * @param string $content The shortcode content.
 * @param string $shortcode_name The name of the shortcode.
 *
 * @since 1.3.0
 *
 * @return string
 */
function process_the_shortcode( array $config, array $attributes, $content, $shortcode_name)
{
    $attributes['id'] = (int) $attributes['id'];

    if ( $attributes['id'] < 1 && ! $attributes['topic'] ) {
        return '';
    }

    ob_start();

    if ( $attributes['id'] > 0 ) {
        render_single_faq( $attributes, $config );
    } else {
        render_topic_faqs( $attributes, $config );
    }

    return ob_get_clean();
}

/**
 * Renders a single FAQ.
 *
 * @param array $attributes
 * @param array $config
 *
 * @since 1.3.0
 *
 * @return void
 */
function render_single_faq(array $attributes, array $config)
{
    $faq = get_post( $attributes['id'] );

    if ( ! $faq) {
        return render_none_found_message($attributes);
    }

    $post_title = $faq->post_title;
    $hidden_content = do_shortcode($faq->post_content);
    $source = "";

    $use_term_container = false;

    include $config['view']['container_single'];
}

/**
 * Render all the FAQs for a topic.
 *
 * @param array $attributes
 * @param array $config
 *
 * @since 1.3.0
 *
 * @return void
 */
function render_topic_faqs(array $attributes, array $config)
{
    $args = array(
        'posts_per_page' => (int) $attributes['number_of_faqs'],
        'post_type' => 'faq',
        'nopaging' => true,
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'tax_query' => array(
            array(
                'taxonomy' => 'topic',
                'field' => 'slug',
                'terms' => $attributes['topic'],
            )
        )
    );

    $query = new \WP_Query($args);

    if (!$query->have_posts()) {
        return render_none_found_message($attributes, false);
    }

    $use_term_container = true;
    $source = 'shortcode-by-topic';
    $term_slug = $attributes['topic'];

    include $config['view']['container_topic'];

    \wp_reset_postdata();
}

/**
 * Render a message when no FAQs are found.
 *
 * @param array $attributes
 * @param boolean $is_single_faq
 *
 * @since 1.0.0
 *
 * @return void
 */
function render_none_found_message(array $attributes, $is_single_faq = true)
{
    if (!$attributes['show_none_found_message']) {
        return;
    }

    $message =  $is_single_faq
        ? $attributes['none_found_single']
        : $attributes['none_found_by_topic'];

    echo "<p>{$message}</p>";
}

/**
 * Loop through query and render FAQs by topic.
 *
 * @param \WP_Query $query
 * @param array $attributes
 * @param array $config
 *
 * @since 1.3.0
 *
 * @return void
 */
function loop_and_render_faqs_by_topic( \WP_Query $query, array $attributes, array $config)
{
    while ($query->have_posts()) {
        $query->the_post();

        $post_title = get_the_title();
        $hidden_content = do_shortcode(get_the_content());

        include $config['view']['faq'];
    }
}
