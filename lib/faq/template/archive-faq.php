<?php

/**
 * FAQ Archive Template
 *
 * @package     GroffTech\Module\FAQ\Template
 * @since       1.3.0
 * @author      Brett Groff (groffTECH)
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\FAQ\Template;

\remove_action('genesis_loop', 'genesis_do_loop');
\add_action('genesis_loop', __NAMESPACE__ . '\do_faq_archive_loop');
/**
 * Render the FAQs
 *
 * @since 1.0.0
 *
 * @return void
 */
function do_faq_archive_loop()
{
    $records = get_posts_grouped_by_term('faq', 'topic');

    if (!$records) {
        echo '<p>No FAQs found.</p>';
    }

    $use_term_container = true;
    $source = 'template';
    $show_term_name = true;

    foreach ($records as $record) {
        $term_slug = $record['term_slug'];
        include(FAQ_MODULE_DIR . '/views/container.php');
    }
}

/**
 * Loop and render FAQs.
 *
 * @param array $faqs
 *
 * @since 1.0.0
 *
 * @return void
 */
function loop_and_render_faqs(array $faqs)
{
    $attributes = array(
        'show_icon' => 'dashicons dashicons-arrow-down-alt2',
        'hide_icon' => 'dashicons dashicons-arrow-up-alt2'
    );

    foreach ($faqs as $faq) {
        $hidden_content = do_shortcode($faq['post_content']);
        $post_title = $faq['post_title'];

        include(FAQ_MODULE_DIR . '/views/faq.php');
    }
}

\genesis();
