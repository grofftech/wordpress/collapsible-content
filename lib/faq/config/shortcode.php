<?php
/**
 * FAQ Shortcode Configuration
 *
 * @package     GroffTech\Module\FAQ\Config
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\FAQ\Config;

return array(
    /*======================================
     * The shortcode name [faq].
     *======================================*/
    'shortcode_name' => 'faq',

    /*======================================
     * A flag to indicate if we want to run
     * do_shortcode() inside of the original
     * shortcode
     *=================.=====================*/
    'do_shortcode_within_content' => false,

    /*======================================
     * Specify a function when you want your
     * code to handle the output buffer, view
     * and processing.
     *======================================*/
    'processing_function' => __NAMESPACE__ . 'process_the_faq_shortcode',

    /*======================================
     * Paths to the view files
     *======================================*/
    'view' => array(
        'container_single'  => FAQ_MODULE_DIR . '/views/container.php',
        'container_topic'   => FAQ_MODULE_DIR . '/views/container.php',
        'faq'               => FAQ_MODULE_DIR . '/views/faq.php',
    ),

    /*======================================
     * Defined shortcode default attributes.
     * These can be overridden by the author
     * in content
     *======================================*/
    'defaults' => array(
        'show_icon'                 => 'dashicons dashicons-arrow-down-alt2',
        'hide_icon'                 => 'dashicons dashicons-arrow-up-alt2',
        'id'                        => 0,
        'topic'                     => '',
        'number_of_faqs'            => -1,
        'show_none_found_message'   => false,
        'none_found_by_topic'       => __('Sorry no FAQs were found for that topic', FAQ_MODULE_TEXT_DOMAIN),
        'none_found_single'         => __('Sorry no FAQ found', FAQ_MODULE_TEXT_DOMAIN)
    ),
);