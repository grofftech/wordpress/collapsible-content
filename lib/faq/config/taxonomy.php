<?php
/**
 * Runtime configuration for custom taxonomies.
 *
 * @package     GroffTech\Module\FAQ\Config
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\FAQ\Config;

return array(

    /*======================================
     * The taxonomy name
     *======================================*/
    'taxonomy' => 'topic',

    /*======================================
     * The label configurations
     *======================================*/
    'labels' => array(
        'custom_type'           => 'topic',
        'singular'              => 'Topic',
        'plural'                => 'Topics',
        'in_sentence_plural'    => 'topics',
        'in_sentence_singular'  => 'topic',
        'text_domain'           => FAQ_MODULE_TEXT_DOMAIN
    ),

    /*======================================
     * The registration arguments
     * See https://codex.wordpress.org/Function_Reference/register_taxonomy
     * for additional labels
     *======================================*/
    'args' => array(
        'label'             => \__('Topics', FAQ_MODULE_TEXT_DOMAIN),
        'labels'            => '', // automatically generate labels
        'hierarchical'      => true,
        'show_admin_column' => true,
        'show_in_rest'      => true, // needed in order to show in block editor
        'public'            => false,
        'show_ui'           => true
    ),


    /*======================================
     * The post types to bind taxonomy to.
     *======================================*/
    'post_types' => array( 'faq' ),
);