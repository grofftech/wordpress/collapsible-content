<?php
/**
 * Runtime configurations for custom post types.
 *
 * @package     GroffTech\Module\FAQ\Config
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\FAQ\Config;

return array(
    /*======================================
     * The post type name
     *======================================*/
    'post_type' => 'faq',

    /*======================================
     * The label configurations
     *======================================*/
    'labels' => array(
        'custom_type' => 'faq',
        'singular' => 'FAQ',
        'plural' => 'FAQs',
        'in_sentence_plural' => 'faqs',
        'in_sentence_singular' => 'faq',
        'text_domain' => FAQ_MODULE_TEXT_DOMAIN,
        'specific_labels' => array()
    ),

    /*======================================
     * Supported features
     *======================================*/
    'features' => array(
        'base_post_type' => 'post',
        'exclude' => array(
            'excerpt',
            'comments',
            'trackbacks',
            'custom-fields'
        ),
        'additional' => array(
            'page-attributes'
        )
    ),

    /*======================================
     * Registration arguments
     *======================================*/
    'args' => array(
        'description'   => 'Frequently Asked Questions (FAQ)',
        'label'         => '',
        'public'        => true,
        'labels'        => '', // automatically generate labels
        'supports'      => '', // automatically generate supports
        'menu_position' => 5,
        'menu_icon'     => 'dashicons-editor-help',
        'has_archive'   => true,
        'show_in_rest'  => true
    )
);