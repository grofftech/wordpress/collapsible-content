<?php
/**
 * Plugin specific functionality
 *
 * @package     GroffTech\CollapsibleContent
 * @author      Brett Groff
 * @since       1.3.0
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\CollapsibleContent;

use GroffTech\Module\Custom as CustomModule;

\add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueue_assets' );
/**
* Loads asset files (CSS, JS, etc.).
*
* @since 1.3.0
*
* @return void
*/
function enqueue_assets() {
    \wp_enqueue_style('dashicons');

    \wp_enqueue_script(
        'collapsible-content-script',
        COLLAPSIBLE_CONTENT_URL . 'assets/dist/js/plugin.js',
        array(),
        '1.3.0',
        true);
}

// \add_action( 'plugins_loaded', __NAMESPACE__ . '\setup_plugin' );
// /**
// * Setup Plugin
// *
// * @since 1.3.0
// *
// * @return void
// */
// function setup_plugin() {
//     foreach (array( 'qa', 'teaser') as $shortcode) {
//         CustomModule\register_shortcode( COLLAPSIBLE_CONTENT_DIR . "/config/shortcode/${shortcode}.php");
//     }
// }
