<?php
/**
 *
 * Custom Module Handler
 *
 * @package     GroffTech\Module\Custom
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\Custom;

/**
 * Autoload files.
 *
 * @since 1.0.0
 *
 * @return void
 */
function autoload()
{
    $files = [
        'label-generator.php',
        'post-type.php',
        'shortcode.php',
        'taxonomy.php',
    ];

    foreach ($files as $files) {
        include __DIR__.'/'.$file;
    }
}

/**
 * Register a plugin with the Custom Module.
 *
 * @since 1.0.0
 *
 * @param string $plugin_file
 *
 * @return void
 */
function register_plugin($plugin_file)
{
    \register_activation_hook($plugin_file, __NAMESPACE__.'\delete_rewrite_rules');
    \register_deactivation_hook($plugin_file, __NAMESPACE__.'\delete_rewrite_rules');
    \register_uninstall_hook($plugin_file, __NAMESPACE__.'\delete_rewrite_rules');
}

/**
 * Delete the rewrite rules from the database.
 *
 * @since 1.0.0
 *
 * @return void
 */
function delete_rewrite_rules()
{
    \delete_option('rewrite_rules');
}
