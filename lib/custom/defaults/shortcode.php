<?php
/**
 * Default configuration parameters for a shortcode.
 *
 * @package     GroffTech\Module\Custom\Defaults
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\Custom\Defaults;

return array(
    /*======================================
     * The shortcode name (within the square brackets)
     *======================================*/
    'shortcode_name' => '',

    /*======================================
     * A flag to indicate if we want to run
     * do_shortcode() inside of the original
     * shortcode.
     *=================.=====================*/
    'do_shortcode_within_content' => true,

    /*======================================
     * Specify a function when you want your
     * code to handle the output buffer, view
     * and processing. Defaults to null.
     *======================================*/
    'processing_function' => null,

    /*======================================
     * Directory path to the view file.
     * For multiple view files, provide an array
     * of directory paths.
     *======================================*/
    'view' => '',

    /*======================================
     * Defined shortcode default attributes.
     * These can be overridden by the author
     * in content.
     *======================================*/
    'defaults' => array()
);