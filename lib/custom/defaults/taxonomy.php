<?php
/**
 * Default configuration parameters for custom taxonomy.
 *
 * @package     GroffTech\Module\Config\Defaults
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\Config\Defaults;

return array(

    /*======================================
     * The taxonomy name
     *======================================*/
    'taxonomy' => '',

    /*======================================
     * The label configurations
     *======================================*/
    'labels' => array(
        'custom_type'           => '', // The taxonomy name from above
        'singular'              => '',
        'plural'                => '',
        'in_sentence_plural'    => '', // The plural for use in a sentence
        'in_sentence_singular'  => '', // The singular for use in a sentence
        'text_domain'           => '',
    ),

    /*======================================
     * The registration arguments
     * See https://codex.wordpress.org/Function_Reference/register_taxonomy
     * for additional labels
     *======================================*/
    'args' => array(
        'label'     => '',
        'labels'    => '', // automatically generate labels
    ),

    /*======================================
     * The post types to bind taxonomy to.
     *======================================*/
    'post_types' => array( '' ),
);