<?php
/**
 * Default configuration parameters for a custom post type.
 *
 * @package     GroffTech\Module\Custom\Defaults
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\Custom\Defaults;

return array(
    /*======================================
     * The post type name
     *======================================*/
    'post_type' => '',

    /*======================================
     * The label configurations
     *======================================*/
    'labels' => array(
        'custom_type'           => '', // The post type from above
        'singular'              => '',
        'plural'                => '',
        'in_sentence_plural'    => '', // The plural for use in a sentence
        'in_sentence_singular'  => '', // The singular for use in a sentence
        'text_domain'           => '',
        'specific_labels'       => array()
    ),

    /*======================================
     * Supported features
     *======================================*/
    'features' => array(
        'base_post_type' => 'post',
        'exclude' => array(), // The features of base post type you want to exclude
        'additional' => array() // Additional features not part of base post type
    ),


    /*======================================
     * Registration arguments
     *======================================*/
    'args' => array(
        'description'   => '',
        'label'         => '',
        'public'        => true,
        'labels'        => '', // automatically generate labels
        'supports'      => '', // automatically generate supports
        'menu_position' => 5,
        'menu_icon'     => '',
        'has_archive'   => true,
        'show_in_rest'  => true // For showing in block editor interface
    )
);