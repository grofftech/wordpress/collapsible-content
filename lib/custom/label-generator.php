<?php

/**
 *
 * Label Generator
 *
 * @package     GroffTech\Module\Custom
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\Custom;

/**
 * Generate labels for custom post type or taxonomy.
 *
 * @since 1.0.0
 *
 * @param array $config An array of configuration parameters.
 * @param string $custom_type The custom type for label generation, default is post_type.
 *
 * @return array
 */
function generate_custom_labels(array $config, $custom_type_name = 'post_type')
{
    // Defaults
    $config = array_merge(
		array(
			'custom_type'       => '',
			'singular_label'    => '',
			'plural_label'      => '',
			'in_sentence_label' => '',
			'text_domain'       => '',
			'specific_labels'   => array(),
		),
		$config
    );

    // Shared labels
    $labels = array(
        'name'                  => \_x(
            $config['plural'],
            "{$custom_type_name} general name",
            $config['text_domain']),
        'singular_name'         => \_x(
            $config['singular'],
            "{$custom_type_name} singular name",
            $config['text_domain']),
        'menu_name'             => \_x($config['plural'], 'admin_menu', $config['text_domain']),
        'add_new_item'          => \__("Add New {$config['singular']}", $config['text_domain']),
        'edit_item'             => \__("Edit {$config['singular']}", $config['text_domain']),
        'view_item'             => \__("View {$config['singular']}", $config['text_domain']),
        'search_items'          => \__("Search {$config['plural']}", $config['text_domain']),
        'parent_item_colon'     => \__("Parent {$config['singular']}:", $config['text_domain']),
        'not_found'             => \__("No {$config['in_sentence_plural']} found.", $config['text_domain']),

    );

    // Determine which type of labels to generate
    $custom_type_generator = __NAMESPACE__;
    $custom_type_generator .= $custom_type_name == 'taxonomy'
		? '\generate_labels_for_taxonomy'
		: '\generate_labels_for_post_type';

    // Combine the labels
	$labels = array_merge(
		$labels,
		$custom_type_generator( $config )
    );

    // Override labels if any specific ones are provided
    if ( $config['specific_labels'] ) {
		$labels = array_merge(
			$labels,
			$config['specific_labels']
		);
	}

	return $labels;
}

/**
* Generate labels for custom taxonomy.
*
* @since 1.0.0
*
* @param array $config An array of configuration parameters.
*
* @return array
*/
function generate_labels_for_taxonomy( $config ) {
    return array(
        'popular_items'              => \__( "Popular {$config['plural']}", $config['text_domain'] ),
        'all_items'                  => \__( "All {$config['plural']}", $config['text_domain'] ),
        'parent_item'                => null,
        'update_item'                => __( "Update {$config['singular']}", $config['text_domain'] ),
        'new_item_name'              => __( "New {$config['plural']} Name", $config['text_domain'] ),
        'separate_items_with_commas' => __(
            "Separate {$config['plural']} with commas",
            $config['text_domain'] ),
        'add_or_remove_items'        => __(
            "Add or remove {$config['plural']}",
            $config['text_domain'] ),
        'choose_from_most_used'      => __(
            "Choose from the most used {$config['plural']}",
            $config['text_domain'] ),
        'back_to_items'             => __( "Back to {$config['plural']}", $config['text_domain'] ),
    );
}

/**
* Generate labels for custom post type.
*
* @since 1.0.0
*
* @param array $config An array of configuration parameters.
*
* @return array
*/
function generate_labels_for_post_type( $config ) {
    return array(
        'name_admin_bar'            => \_x(
             $config['singular'], 'add_new_on_toolbar',
             $config['text_domain'] ),
        'add_new'                   => \_x( "Add New {$config['custom_type']}", $config['text_domain'] ),
        'new_item'                  => \__( "New {$config['singular']}", $config['text_domain'] ),
        'all_items'                 => \__( "All {$config['plural']}", $config['text_domain'] ),
        'not_found_in_trash'        => \__(
            "No {$config['in_sentence_plural']} found in Trash.",
            $config['text_domain'] ),
        'archives'                  => \__( "{$config['plural_label']} Archives", $config['text_domain'] ),
        'attributes'                => \__( "{$config['plural_label']} Attributes", $config['text_domain'] ),
        'insert_into_item'          => \__( "Insert into {$config['in_sentence_singular']}.", $config['text_domain'] ),
        'uploaded_to_this_item'     => \__(
            "Uploaded to this {$config['in_sentence_singular']}.",
            $config['text_domain'] ),
        'item_published'            => \__( "{$config['singular']} published.", $config['text_domain'] ),
        'item_published_privately'  => \__(
            "{$config['singular']} published privately.",
            $config['text_domain'] ),
        'item_reverted_to_draft'    => \__(
            "{$config['singular']} reverted to draft.",
            $config['text_domain'] ),
        'item_scheduled'            => \__( "{$config['singular']} scheduled.", $config['text_domain'] ),
        'item_updated'              => \__( "{$config['singular']} updated.", $config['text_domain'] ),
    );
}