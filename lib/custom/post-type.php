<?php

/**
 * Custom Post Type Handler
 *
 * This is a code generator that handles building the config, arguments and registering the post type with WordPress.
 *
 * @package     GroffTech\Module\Custom
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\Custom;

\add_action('init', __NAMESPACE__ . '\register_custom_post_types');
/**
 * Registers the custom post types.
 *
 * @since 1.0.0
 *
 * @return void
 */
function register_custom_post_types()
{
    $configs = array();
    /**
	 * Add custom post type configurations for generating and
	 * registering with WordPress.
	 *
	 * @since 1.0.0
	 *
	 * @param array Array of configurations.
	 */
    $configs = (array) \apply_filters( 'add_custom_post_type_config', $configs );

    foreach ($configs as $post_type => $config) {
        register_custom_post_type($post_type, $config);
    }
}

/**
 * Registers custom post type.
 *
 * @since 1.0.0
 *
 * @param string $post_type The name of the custom post type.
 * @param array $config The configuration for the custom post type.
 *
 * @return void
 */
function register_custom_post_type($post_type, array $config)
{
    $args = $config['args'];

    if ( ! $args['supports'] ) {
        $args['supports'] = generate_post_type_features( $config['features'] );
    }

    if ( ! $args['labels'] ) {
        $args['labels'] = generate_custom_labels( $config['labels'] );
    }

    \register_custom_post_type($post_type, $args);
}

/**
 * Generate all the supported features for a custom post type.
 *
 * @since 1.0.0
 *
 * @param array $features The features configuration for the custom post type.
 *
 * @return array The supported features for the custom post type.
 */
function generate_post_type_features(array $config)
{
    $base_post_type_features = \get_all_post_type_supports( $config['base_post_type'] );
    $supported_features = exclude_post_type_features( $base_post_type_features, $config['exclude'] );
    $supported_features = merge_post_type_features( $supported_features, $config['additional'] );

    return $supported_features;
}

/**
 * Excludes post type features.
 *
 * @since 1.0.0
 *
 * @param array $supported_features The supported features for a post type.
 * @param array|string $exclude_features The features to exclude.
 *
 * @return array The supported features for the post type.
 */
function exclude_post_type_features(array $supported_features, array $exclude_features)
{
    if ( !$exclude_features ) {
        return array_keys( $supported_features );
    }

    // Build the supported features, skip if feature is in excluded features
    $features = array();
    foreach ( $supported_features as $feature ) {
        if ( in_array( $feature, $exclude_features ) ) {
            continue;
        }

        $features[] = $feature;
    }

    return $features;
}

/**
 * Merges supported post type features.
 *
 * @since 1.0.0
 *
 * @param array $supported_features The supported features for the post type.
 * @param array $additional_features The additional features to add.
 *
 * @return array The supported features for the post type.
 */
function merge_post_type_features(array $supported_features, $additional_features)
{
    if ( ! $additional_features ) {
        return $supported_features;
    }

    return array_merge( $supported_features, $additional_features );
}
