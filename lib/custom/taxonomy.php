<?php
/**
 * Custom Taxonomy Handler
 *
 * This is a code generator that handles building the labels, arguments and registering the taxonomy with WordPress.
 *
 * @package     GroffTech\Module\Custom
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace GroffTech\Module\Custom;

\add_action('init', __NAMESPACE__ . '\register_custom_taxonomies');
/**
 * Register the custom taxonomies.
 *
 * @since 1.0.0
 *
 * @return void
 */
function register_custom_taxonomies() {
	$configs = array();
	/**
	 * Add taxonomy configurations for generating and
	 * registering each with WordPress.
	 *
	 * @since 1.0.0
	 *
	 * @param array Array of configurations.
	 *
	 * @return array
	 */
	$configs = (array) \apply_filters( 'add_custom_taxonomy_config', $configs );

	foreach ( $configs as $taxonomy => $config ) {
		register_custom_taxonomy( $taxonomy, $config );
	}
}
/**
 * Register the taxonomy.
 *
 * @since 1.0.0
 *
 * @param string $taxonomy Taxonomy name to be registered with WordPress
 * @param array $config An array of taxonomy runtime configuration parameters.
 *
 * @return void
 */
function register_custom_taxonomy( $taxonomy, array $config ) {
	$args = $config['args'];

	if ( ! $args['labels'] ) {
		$args['labels'] = generate_custom_labels( $config['labels'], 'taxonomy' );
	}

	\register_taxonomy( $taxonomy, $config['post_types'], $args );
}
